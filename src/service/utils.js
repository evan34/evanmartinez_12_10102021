/**
 * Create kilo and calo values arrays
 * @param {Object} data 
 * @returns Object of arrays
 */
export const changeActivityData = data => {
    const sessions = { kilograms: [], calories: [] };
    data.sessions.map((session, index) => {
      session.day = index + 1;
      sessions.kilograms.push(session.kilogram);
      sessions.calories.push(session.calories);
      return sessions
    });
  return sessions;
}

/**
 * Create object of french days and length of sessions
 * @param {Object} data 
 * @returns Object
 */
export const changeAverageSessionsData = data => {
  const days = ["L", "M", "M", "J", "V", "S", "D"];
  const sessions = data.sessions.map((session, index) => {
    return {
      name: days[index],
      sessionLength: session.sessionLength
    }
  });
  return sessions;
}

/**
 * Change case, order of kinds data
 * @param {Object} data 
 */
export const changeRadarKindData = data => {
  const polarAngleAxisLabels = Object.values(data.kind)
    .map(el => el.charAt(0).toUpperCase() + el.slice(1)).reverse();
  
  data.data.forEach((obj, i) => {
    obj['kind'] = polarAngleAxisLabels[i];
  });
}