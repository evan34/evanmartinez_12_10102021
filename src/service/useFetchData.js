import axios from "axios";
import { useEffect, useState } from "react";

/**
 * @param {String} url 
 * @returns data
 */
export const useFetchData = (url) => {
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(false);
  const [data, setdata] = useState({});

  useEffect(() => {
     function getData() {
      axios.get(url)
      .then(res => {
        setdata(res.data.data);
        setIsLoading(false);
      })
      .catch(err => {
        setIsLoading(false);
        setError(err.message);
      })
      .finally(() => {
        setIsLoading(false);
      })
    };
    getData();
  }, [url, isLoading]);

  return {
    isLoading,
    error,
    data
  };
};