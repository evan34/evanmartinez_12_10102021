import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';

import Dashboard from './components/Dashboard';
import Error from './components/Error';
import TopNav from './components/TopNav';
import Aside from './components/Aside';

import './App.css';

function App() {

  return (
    <Router>
      <TopNav />
      <main className="mainContainer">
        <Aside />
        <Switch>
          <Route exact path="/user/:id" component={Dashboard} />
          <Route to="*" component={Error} />
        </Switch>
      </main>
    </Router>
  );
}

export default App;
