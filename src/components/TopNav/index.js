import React from 'react';
import logo from '../../assets/img/logo.png';

const TopNav = () => {
  return (
    <div className="topNavContainer">
      <div className="topNavContainer__logo">
        <img src={logo} alt='logo' />
      </div>
      <div className="topNavContainer__nav">
        <ul>
          <li>Accueil</li>
          <li>Profil</li>
          <li>Réglage</li>
          <li>Communauté</li>
        </ul>
      </div>

    </div>
  )
}

export default TopNav
