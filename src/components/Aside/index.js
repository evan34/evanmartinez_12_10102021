import React from 'react';
import { Link } from 'react-router-dom';
import activity1 from '../../assets/img/activity1.png';
import activity2 from '../../assets/img/activity2.png';
import activity3 from '../../assets/img/activity3.png';
import activity4 from '../../assets/img/activity4.png';

/**
 * @returns HTML element
 */
const Aside = () => {
  return (
    <aside className="asideContainer">
      <div className="asideContainer__icons">
        <div className="asideContainer__icon">
          <Link to="#"><img src={activity1} alt="activity" /></Link>
        </div>
        <div className="asideContainer__icon">
          <Link to="#"><img src={activity2} alt="activity" /></Link>
        </div>
        <div className="asideContainer__icon">
          <Link to="#"><img src={activity3} alt="activity" /></Link>
        </div>
        <div className="asideContainer__icon">
          <Link to="#"><img src={activity4} alt="activity" /></Link>      
        </div>
      </div>
      <div className="asideContainer__copyrights">
        <p>Copiryght, SportSee 2020</p>
      </div>
    </aside>
  )
}

export default Aside
