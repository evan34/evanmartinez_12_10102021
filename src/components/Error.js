import React from 'react';

const Error = () => {
  return (
    <div className="container">
      <p className="container__error">404</p>
      <p className="container__oups">Oups! La page que vous demandez n'existe pas.</p>
    </div>
  )
}

export default Error;