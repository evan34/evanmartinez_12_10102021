import { useFetchData } from '../../../../service/useFetchData';
import { BASE_URL } from '../../../../service/constants';
import { changeAverageSessionsData } from '../../../../service/utils';

import {
  ResponsiveContainer,
  LineChart,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  Line
} from 'recharts'

import ChartLineTooltip from './ChartLineTooltip';
import PropTypes from 'prop-types';

/**
 * @param {String} /user/:id 
 * @returns HTML element
 */
const ChartLine = ({url}) => {
  const { isLoading, error, data} =
    useFetchData(`${BASE_URL}${url}/average-sessions`);

  let sessions;
  let sessionsLength = [];
  let minMaxSessions = [];
  const getSessions = () => {
    sessions = changeAverageSessionsData(data);
    sessions.map(session => sessionsLength.push(session.sessionLength));
    minMaxSessions.push(Math.min(...sessionsLength));
    minMaxSessions.push(Math.max(...sessionsLength));
  }

  return (
    <div className="chartLineContainer">
      {isLoading ? (
        <p>Chargement...</p>
      ) : (
        <>
          {error ? (
            <p>Impossible d'afficher les données !</p>  
          ) : (
            <>
              {getSessions()}
              <h2 className='chartLine-title'>Duree moyenne des sessions</h2>
              <ResponsiveContainer width="100%" height="100%">
                <LineChart
                  width={260}
                  height={130}
                  data={sessions}
                  margin={{ top: 5, right: 0, left: 0, bottom: 5 }}
                >
                  <CartesianGrid
                    fill="#FF0000"
                    horizontal={false}
                    vertical={false}
                  />
                  <XAxis
                    dataKey="name"
                    tick={{ fontSize: 12 }}
                    stroke="#FFFFFF80"
                    padding={{ left: 10, right: 10 }}
                    tickLine={false}
                    axisLine={false}
                  />
                  <YAxis
                    domain={[minMaxSessions[0], minMaxSessions[1]]}
                    hide={true}
                    padding={{ top: 75 }}
                  />
                  <Tooltip
                    cursor={{ fill: '#000' }}
                    content={<ChartLineTooltip />}
                  />
                  <Line
                    type="monotone"
                    dataKey="sessionLength"
                    stroke="#FFFFFFCC"
                    strokeWidth={2}
                  />
                </LineChart>
              </ResponsiveContainer>
            </>
          )}
        </>
      )}
    </div>
  )
}

ChartLine.propTypes = {
  url: PropTypes.string
}

export default ChartLine;