import React from 'react'
import PropTypes from "prop-types";

/**
 * @param {Boolean} active 
 * @param {Array} payload 
 * @returns 
 */
const ChartLineTooltip = ({ active, payload }) => {
  if (active && payload && payload.length) {
    return (
      <div className="chartLine-tooltip">
        { payload.map((el, index) => (
            <p className="label" key={index}>{`${el.value} min`}</p>
          ))
        }
      </div>
    );
  }
  return null;
}

ChartLineTooltip.propTypes = {
  active: PropTypes.bool,
  payload: PropTypes.array
};

export default ChartLineTooltip;
