import { useFetchData } from '../../../../service/useFetchData';
import { BASE_URL } from '../../../../service/constants';


import {
  ResponsiveContainer,
  RadialBarChart,
  RadialBar,
  Legend,
} from 'recharts';
import PropTypes from 'prop-types';

/**
 * @param {String} /user/:id 
 * @returns HTML element
 */
const ChartRadialBar = ({url}) => {
  const { isLoading, error, data} =
    useFetchData(`${BASE_URL}${url}`);

  const userScore = [{
    name: 'Score',
    value: data.todayScore * 100
  }];

  return (
    <div className="chartRadialBarContainer">
      {isLoading ? (
        <p>Chargement...</p>
      ) : (
        <>
          {error ? (
            <p>Impossible d'afficher les données !</p>  
          ) : (
            <>
              <div className="radialBarComment">
                <div className="percent"> {userScore[0].value}%</div>
                <p className="comment">de votre objectif</p>
              </div>
              <ResponsiveContainer width="100%" height="100%">
                <RadialBarChart
                    cx="50%"
                    cy="50%"
                    innerRadius="60%"
                    //outerRadius={140}
                    barSize={10}
                    startAngle={80} 
                    endAngle={220}
                    data={userScore}
                >
                  <RadialBar
                    minAngle={15}
                    cornerRadius={50}
                    dataKey='value'
                    clockWise
                    fill='#E60000'
                  />
                  <Legend
                    iconSize={0}
                    width={120}
                    height={140}
                    layout='vertical'
                    verticalAlign='middle'
                    align="left"
                    fill='#282D30' 
                  />
                </RadialBarChart>
              </ResponsiveContainer>
            </>
          )}
        </>
      )}
    </div>
  )
}

ChartRadialBar.propTypes = {
  url: PropTypes.string
}

export default ChartRadialBar;