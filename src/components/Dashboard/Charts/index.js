import React from 'react';
import ChartBar from './ChartBar';
import ChartLine from './ChartLine';
import ChartRadar from './ChartRadar';
import ChartRadialBar from './ChartRadialBar';

/**
 * @param {Object} /user/:id 
 * @returns HTML element
 */
const Charts = (props) => {
  return (
    <div className="chartsContainer">
      <div className="chartsContainer__topBar">
        <ChartBar url={ props.url }/>
      </div>
      <div className="chartsContainer__bottomBars">
        <ChartLine url={ props.url } />
        <ChartRadar url={ props.url } />
        <ChartRadialBar url={ props.url } />
      </div>
    </div>
  )
}

export default Charts;

