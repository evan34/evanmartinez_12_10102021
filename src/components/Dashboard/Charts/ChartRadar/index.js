import { useFetchData } from '../../../../service/useFetchData';
import { BASE_URL } from '../../../../service/constants';
import { changeRadarKindData } from '../../../../service/utils';

import {
  ResponsiveContainer,
  RadarChart,
  PolarGrid,
  PolarAngleAxis,
  Radar,
} from 'recharts'
import PropTypes from 'prop-types';

/**
 * @param {String} /user/:id 
 * @returns HTML element
 */
const ChartRadar = ({url}) => {
  const { isLoading, error, data } =
    useFetchData(`${BASE_URL}${url}/performance`);
  
  return (
    <div className="chartRadarContainer">
      {isLoading ? (
        <p>Chargement...</p>
      ) : (
        <>
          {error ? (
            <p>Impossible d'afficher les données !</p>  
            ) : (
            <>      
              {changeRadarKindData(data)}
              <ResponsiveContainer width="100%" height="100%">
                <RadarChart
                  outerRadius="70%"
                  cx="50%"
                  cy="50%"
                  data={data.data}
                >
                  <PolarGrid />
                  <PolarAngleAxis
                    dataKey="kind"
                    tickLine={false}
                    stroke="#FFF"
                    tick={{ fontSize: 12 }}
                  />
                  <Radar
                    dataKey="value"
                    stroke="#FF0101"
                    fill="#FF0101"
                    fillOpacity={0.7}
                  />
                </RadarChart>
              </ResponsiveContainer>
            </>
          )}
        </>
      )}
    </div>
  )
}

ChartRadar.propTypes = {
  url: PropTypes.string
}

export default ChartRadar;