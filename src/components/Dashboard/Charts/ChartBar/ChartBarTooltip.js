import React from 'react';
import PropTypes from "prop-types";

/**
 * @param {Boolean} active 
 * @param {Array} payload 
 * @returns 
 */
const ChartBarTooltip = ({ active, payload }) => {
  if (active && payload && payload.length) {
    return (
      <div className="customTooltip">
        <p className="customTooltip__value">{`${payload[0].value}`}Kg</p>
        <p className="customTooltip__value">{`${payload[1].value}`}Kcal</p>
      </div>
    );
  }
  return null;
}

ChartBarTooltip.propTypes = {
  active: PropTypes.bool,
  payload: PropTypes.array
};

export default ChartBarTooltip;
