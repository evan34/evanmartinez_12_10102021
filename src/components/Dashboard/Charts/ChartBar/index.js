import { useFetchData } from '../../../../service/useFetchData';
import { BASE_URL } from '../../../../service/constants';
import { changeActivityData } from '../../../../service/utils';

import ChartBarTooltip from './ChartBarTooltip';
import {
  BarChart,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  Bar,
  ResponsiveContainer
} from 'recharts';

import PropTypes from 'prop-types';

/**
 * @param {String} /user/:id 
 * @returns HTML element
 */
const ChartBar = ({url}) => {
  const { isLoading, error, data } =
    useFetchData(`${BASE_URL}${url}/activity`);

  let minMaxSessions = [];
  /**
   * Get minimum and maximum 
   * kilograms and calories data
   */
  const sessionsCalculated = () => {
    const sessions = changeActivityData(data);
    console.log(sessions)
    minMaxSessions.push(Math.min(...sessions.kilograms) - 4);
    minMaxSessions.push(Math.max(...sessions.kilograms) + 4);
    minMaxSessions.push(Math.min(...sessions.calories) - 10);
    minMaxSessions.push(Math.max(...sessions.calories) + 10);
  }

  return (
    <div className='chartBarContainer'>
      {isLoading ? (
        <p>Chargement...</p>
      ) : (
        <>
          {error ? (
           <p>Impossible d'afficher les données !</p>   
            ) : (
                <>
              {sessionsCalculated()}   
              <div className="chartBarContainer--header">
                <h3>Activité quotidienne</h3>
                <div className="labelsActivities">
                  <div className="labelsActivities--black">
                    <div className="blackCircle"></div>
                    <p className="weight">Poids</p>
                  </div>
                  <div className="labelsActivities--red">
                    <div className="redCircle"></div>
                    <p className="calories">Calories brûlées</p>
                  </div>
                </div>
                </div>         
                <ResponsiveContainer width="100%" height="90%">
                  <BarChart
                    width={750}
                    height={250}
                    data={data.sessions}
                    barCategoryGap={35}
                    barGap={10}
                    margin={{
                      top: 54,
                      right: 30,
                      left: 20,
                      bottom: 5,
                    }}
                  >
                  <CartesianGrid
                    strokeDasharray="3 3"
                    vertical={false}
                  />
                  <XAxis
                    dataKey="day"
                    tickLine={false}
                    tick={{fontSize: 14, fontWeight: 8}}
                  />
                  <YAxis
                    yAxisId="kilogram"
                    orientation="right"
                    interval="number"
                    domain={[minMaxSessions[0], minMaxSessions[1]]}
                    tickLine={false}
                    tick={{fontSize: 14, fontWeight: 16}}
                  />
                  <YAxis
                    yAxisId="calories"
                    hide={true}
                    domain={[minMaxSessions[2], minMaxSessions[3]]}
                  />
                  <Tooltip
                    content={<ChartBarTooltip />}
                    cursor={{ fill: "#e0e0e0" }}
                  />
                  <Bar
                    yAxisId="kilogram"
                    dataKey="kilogram"
                    fill="#282D30"
                    radius={[50, 50, 0, 0]}
                  />
                  <Bar
                    yAxisId="calories"
                    dataKey="calories"
                    fill="#E60000"
                    radius={[50, 50, 0, 0]}
                  />
                  </BarChart>
                </ResponsiveContainer>
              </>    
          )}    
        </>
      )}
    </div>
  )
}

ChartBar.propTypes = {
  url: PropTypes.string
}

export default ChartBar;
