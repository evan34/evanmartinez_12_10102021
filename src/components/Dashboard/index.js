import React from 'react';

import HeaderChart from './HeaderChart';
import Charts from './Charts';
import CardInfos from "./CardInfos";

/**
 * @param {Object}  
 * @returns HTML element
 */
const Dashboard = (props) => {
  return (
    <section className="dashboard">
      <div className="dashboard__header">
        <HeaderChart url={props.match.url} /> 
      </div>
      <div className="dashboard__charts">
        <Charts url={props.match.url} />
        <CardInfos url={props.match.url} />
      </div>
    </section>
  )
}

export default Dashboard;