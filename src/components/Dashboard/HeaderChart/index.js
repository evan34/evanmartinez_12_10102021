import { useFetchData } from '../../../service/useFetchData';
import { BASE_URL } from '../../../service/constants';

import PropTypes from 'prop-types';

/**
 * @param {String} /user/:id 
 * @returns HTML element
 */
function HeaderChart({ url }) {
  const { isLoading, error, data } =
    useFetchData(`${BASE_URL}${url}`);

  return (
    <header className='headerContainer'>
      {isLoading ? (
        <p>Chargement...</p>
      ) : (
        <>
          {error ? (
            <p>Impossible d'afficher les données !</p>
          ) : (
            <>
              <h1 className='headerContainer__title'>Bonjour <span className='headerContainer__firstName'> {data.userInfos.firstName} </span></h1>
              <p>Félicitations ! Vous avez explosé vos objectifs hier 👏</p>
            </>
          )}
        </>
      )}
    </header>
  );
}

HeaderChart.propTypes = {
  url: PropTypes.string
}

export default HeaderChart;