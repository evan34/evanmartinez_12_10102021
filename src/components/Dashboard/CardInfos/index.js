import { useFetchData } from '../../../service/useFetchData';
import { BASE_URL } from '../../../service/constants';

import calories from '../../../assets/img/calories.png';
import protein from '../../../assets/img/protein.png';
import carbo from '../../../assets/img/carbo.png';
import lipids from '../../../assets/img/lipids.png';

import PropTypes from 'prop-types';

/**
 * @param {String} /user/:id 
 * @returns HTML element
 */
const CardInfos = ({url}) => {
  const { isLoading, error, data } =
    useFetchData(`${BASE_URL}${url}`);
  
  return (
    <div className="cardInfosContainer">
      {isLoading ? (
        <p>Chargement...</p>
      ) : (
        <>
          {error ? (
            <p>Impossible d'afficher les données !</p>
          ) : (
            <>
              <div className="cardInfosContainer__info">
                <div className="cardInfosContainer__info--icon">
                  <img src={calories} alt="calories" />
                </div>
                <div className="cardInfosContainer__info--data">
                  <p>{data.keyData.calorieCount}kCal</p>
                  <h4>Calories</h4>
                </div>
              </div>

              <div className="cardInfosContainer__info">
                <div className="cardInfosContainer__info--icon">
                  <img src={protein} alt="proteines" />
                </div>
                <div className="cardInfosContainer__info--data">
                  <p>{data.keyData.proteinCount}g</p>
                  <h4>Proteines</h4>
                </div>
              </div>

              <div className="cardInfosContainer__info">
                <div className="cardInfosContainer__info--icon">
                  <img src={carbo} alt="glucides" />
                </div>
                <div className="cardInfosContainer__info--data">
                  <p>{data.keyData.carbohydrateCount}g</p>
                  <h4>Glucides</h4>
                </div>
              </div>

              <div className="cardInfosContainer__info">
                <div className="cardInfosContainer__info--icon">
                  <img src={lipids} alt="lipides" />
                </div>
                <div className="cardInfosContainer__info--data">
                  <p>{data.keyData.lipidCount}g</p>
                  <h4>Lipides</h4>
                </div>
              </div>
            </>
          )}
        </>
      )}
    </div>
  );
}

CardInfos.propTypes = {
  url: PropTypes.string
}

export default CardInfos;
